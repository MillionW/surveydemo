package com.huami.effective.entity;

import lombok.Data;

import java.util.Date;



@Data
public class ContinuousIntegrationEntity {

    private Integer id;

    private double value;

    private Date reportTime;

    private String belong;
	
	private String name1;

}
